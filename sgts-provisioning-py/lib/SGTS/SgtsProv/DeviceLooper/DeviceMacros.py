class DeviceMacros:
	def __init__(self,variable):
		self.config = variable['config']
                self.logger = variable['logger']
                self.cmdb_file = variable['cmdb_file']
                self.zbx_api = variable['zbx_api']
                self.db_obj = variable['db_obj']
	
	def create_macros(self,device):
		macroList = []
		for macro_name in self.config['sgts']['csv_macros']:
			macroList.append({'macro':"{$%s}" % macro_name,'value':device[macro_name]})
		return macroList
			
