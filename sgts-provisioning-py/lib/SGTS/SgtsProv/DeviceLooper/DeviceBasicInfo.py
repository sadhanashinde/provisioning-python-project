import re
class DeviceBasicInfo:
	def __init__(self,variable):
		self.config = variable['config']
                self.logger = variable['logger']
                self.cmdb_file = variable['cmdb_file']
                self.zbx_api = variable['zbx_api']
                self.db_obj = variable['db_obj']
		self._device_data = variable['_device_data']
	
	def get_interface_details(self,device):
		self._device_data['host'] = device['DEVICE_NAME']
		self._device_data['name'] = device['DEVICE_NAME']
		self._device_data['interfaces'] = {}
		self._device_data['interfaces']['dns'] =  ""
		self._device_data['interfaces']['ip'] = device['IP_ADDRESS']
		self._device_data['interfaces']['useip'] = 1
		self._device_data['interfaces']['main'] = 1
		self._device_data['interfaces']['bulk'] = 1
		self._device_data['interfaces']['type'] = 1
		t_list = ""
		host_interfaces = self.config['sgts']['host_interfaces']
		for rule in host_interfaces:
			ok = 1
			for field  in rule['criteria']:
				v = rule['criteria'][field]
				dev_field_val = device[field]
				pattern = re.compile(v)
				match =  re.findall(v,r"(?=("+dev_field_val+r"))")
				if not match:
					ok = 0
					t_list = ""
				else:
					t_list = rule['port']
					
			if ok:
				t_list
				self._device_data['interfaces']['port'] = t_list
