import re
class DeviceTemplates:
	def __init__(self,variable):
		self.config = variable['config']
                self.logger = variable['logger']
                self.cmdb_file = variable['cmdb_file']
                self.zbx_api = variable['zbx_api']
                self.db_obj = variable['db_obj']	
		
	def _create_template_list(self,device):
		names = []
		for rule in self.config['sgts']['template_rules']:
			names.append(rule['target'])

		templates = self.zbx_api.template.get({'filter':{'name':names}})
		lists = {}
		for t in templates:
			lists[t['name']] = t

		t_list = []
		for rule in self.config['sgts']['template_rules']:
			ok = 1
			if(rule['target']=='SGTS ICMP Ping'):
				#t_list.append(rule['target'])
				#print lists[rule['target']]
				#t_list.append(rule)
				t_list.append(lists[rule['target']])
			for field in rule['criteria']:
				v = rule['criteria'][field] 
				dev_field_val = device[field]
				pattern = re.compile(v)
				match =  re.findall(v,r"(?=("+dev_field_val+r"))")	
				if not match:
					ok = 0
				else:
					#t_list.append(rule['target'])
					#t_list.append(rule)
					#print lists[rule['target']]
					t_list.append(lists[rule['target']])
		if not t_list:
			self.logger.info("device '%s' does not match any template rule, skipped",device['DEVICE_NAME'])
		return t_list
		
