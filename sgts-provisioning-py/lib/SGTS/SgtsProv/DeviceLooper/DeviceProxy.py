class DeviceProxy:
	def __init__(self,variable):
		self.config = variable['config']
                self.logger = variable['logger']
                self.cmdb_file = variable['cmdb_file']
                self.zbx_api = variable['zbx_api']
                self.db_obj = variable['db_obj']
                #self._device_data = variable['_device_data']	
		
	def _create_proxy_list(self,device):
		lists = []
		for proxy_name in self.config['sgts']['proxies']:
			proxy = self.zbx_api.proxy.get({"filter":{"host":self.config['sgts']['proxies'][proxy_name]}})
			if not proxy:
				self.logger.error("Could not find proxy named '%s'",self.config['sgts']['proxies'][proxy_name])
			lists.append(proxy)
		device_proxy = self.config['sgts']['proxies'][device['SGTS']]
		for proxies in proxy:
			if(proxies['host']==device_proxy):
				proxyid = proxies['proxyid']
		return proxyid
