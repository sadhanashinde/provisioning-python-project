#from DeviceLooper import DeviceCreateOrUpdate
from DeviceLooper import DeviceBasicInfo
from DeviceLooper import DeviceProxy
from DeviceLooper import DeviceGroups
from DeviceLooper import DeviceTemplates
from DeviceLooper import DeviceMacros
from DeviceLooper import DeviceHostInventory
class Device:
	csv = __import__('csv')
	def __init__ (self,variable):
		self.config = variable['config']
		self.logger = variable['logger']
		self.cmdb_file = variable['cmdb_file']
		self.zbx_api = variable['zbx_api']
		self.db_obj = variable['db_obj']
		proxies = DeviceProxy(variable)
		self.proxies = proxies
		groups = DeviceGroups(variable)
		self.groups = groups
		templates = DeviceTemplates(variable)
		self.templates = templates
		macros = DeviceMacros(variable)
		self.macros = macros
		inventory = DeviceHostInventory(variable)
		self.inventory = inventory
		variable['_device_data'] = {}
		self._device_data = variable['_device_data']
		basicInfo = DeviceBasicInfo(variable)
		self.basicInfo = basicInfo
	
	def run(self):
		self._loop_init()
		self._loop_run()
		self._loop_finish()

	def _loop_init(self):
		pass
	
	def _loop_run(self):
		with open(self.cmdb_file, 'rb') as csvfile:
			fp = self.csv.reader(csvfile, delimiter=';', quotechar='|')
			cols = fp.next()
			for  row in fp:
				device = dict(zip(cols,row))
				device['_id'] = fp.line_num
				row_number = device['_id'] - 1
				self.logger.info("'%s' - Start Processing. Device count '%d'",device['DEVICE_NAME'],row_number)	
				self._loop_item_prepare(device)
				self.logger.info("'%s' - Handling device.",device['DEVICE_NAME'])
				self._loop_item_handle(device)
				self._loop_item_done(device)
		
	def _loop_item_prepare(self,device):
		return(self.db_obj.validate_columns_data(device))

	def _loop_item_handle(self,device):
		self.basicInfo.get_interface_details(device)
		self._device_data['proxy_hostid'] = self.proxies._create_proxy_list(device)
		group = self.groups._create_host_group(device)
		self._device_data['groups'] = group
		template_names = self.templates._create_template_list(device)
		self._device_data['templates'] = template_names
		macro_list = self.macros.create_macros(device)
		self._device_data['macros'] = macro_list
		inventories = self.inventory.create_inventory(device)
		self._device_data['inventory_mode'] = inventories['inventory_mode']
		self._create_host(device)
		#exit()
	
	def _create_host(self,device):
		hosts = self.zbx_api.host.get({'filter':{"host":[device['DEVICE_NAME']]}})
		if not hosts:
			self.logger.info("Going For update of host")
		else:
			host = self.zbx_api.host.create(self._device_data)	
			if not host:
				self.logger.info("couldn't create a host")
			else:
				self.logger.info("Host has been Created with Hostid %s",host['hostids'])

	def _loop_item_done(self,device):
		pass

	def _loop_finish(self):
		print "bye"
	
