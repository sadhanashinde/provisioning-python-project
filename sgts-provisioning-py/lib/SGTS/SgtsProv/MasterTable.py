class MasterTable:
	#import pymysql
	def __init__(self,variable):
		self.dbh = variable['dbh']
		self.logger = variable['logger']

	def validate_columns_data(self,device):
		counter = 0
		dbColumns = {'SGT_SITE_CODE':'sgts_site_details','CLASSIFICATION':'sgts_classification','CRITICALITY':'sgts_criticality','SGTS':'sgts_delegation','DEVICE_TYPE':'sgts_device_type','MANUFACTURER':'sgts_manufacturer','OS':'sgts_operating_system'}
		query_count = len(dbColumns)	
		self.logger.info("'%s' - Validating mandatory column data.",device['DEVICE_NAME'])
		for keys, values in device.items():
			for key,value in dbColumns.items():
				if(key == keys):
					if(key == 'SGT_SITE_CODE'):
						query = "select * from "+ value + " where SITE_CODE='"+ values + "'"
					else:
						query = "select * from "+ value + " where name='"+ values + "'"
					self.dbh.execute(query)
					results  = self.dbh.fetchone()
					if(results==None):
						self.logger.error("'%s' - Bad Values found for device,skipped.",device['DEVICE_NAME'])
						self.logger.error("'%s' - Error while handling query '%s', skipped.",device['DEVICE_NAME'],query)
					else:
						counter = counter + 1
						if(key == 'SGT_SITE_CODE'):
							site_details = self.get_site_data(device['SGT_SITE_CODE'])
							for field, val in site_details.items():
								device[field] = val
		if(query_count==counter):
			self.logger.info("'%s' - Validation of mandatory column data is successful.",device['DEVICE_NAME'])
			return 1
		else:
			self.logger.error("'%s' - Validation of mandatory column data failed.",device['DEVICE_NAME'])
			return 0


	def get_site_data(self,site_code):
		query = "SELECT POST_CODE,LOCATION_ADDRESS,SECTOR,SITE_NAME,TIME_ZONE,COUNTRY_NAME,CITY_NAME,ATTACHMENT_ENTITY,ZONE FROM sgts_site_details where SITE_CODE='"+ site_code + "'"
		self.dbh.execute(query)
		results  = self.dbh.fetchone()
		return results
